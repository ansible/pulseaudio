# Pulseaudio configuration

Limited to just one workstation (`asks2`) for now.

After restarting the machine (after upgrading kernel from GA to HWE, but I'm not
sure that was the catalyst), the speakers would buzz and crackle loudly
starting shortly after any playback was stopped.

I managed to figure out that this buzzing occurred whenever the state of the sink
changed from `IDLE` to `SUSPENDED`.
The buzzing continued until any playback was started again.

```
taha@asks2:~
$ pactl list short sinks
0	alsa_output.pci-0000_38_00.6.analog-stereo	module-alsa-card.c	s16le 2ch 44100Hz	IDLE
1	alsa_output.platform-snd_aloop.0.analog-stereo	module-alsa-card.c	s16le 2ch 44100Hz	IDLE
```

I don't understand why the buzzing occurs when suspended (I don't think that should happen in the first place),
but between debugging the hardware or the software, looking for a software-fix appeared easier.

Turns out it is possible to disable `idle-to-suspend`, and it appears to resolve the issue.

+ https://unix.stackexchange.com/questions/114602/pulseaudio-sink-always-suspended
+ https://reddit.com/r/archlinux/comments/fhy0kh/pulseaudio_has_suspendonidle_feature_is/
+ https://www.digi.com/resources/documentation/digidocs/90001546/reference/bsp/cc6/r_audio.htm
+ https://rastating.github.io/setting-default-audio-device-in-ubuntu-18-04/
